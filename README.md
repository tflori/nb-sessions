# NbSessions

[![Build Status](https://gitlab.w00tserver.org/tflori/nb-sessions/badges/master/pipeline.svg)](https://gitlab.w00tserver.org/tflori/nb-sessions/-/pipelines)
[![Coverage Status](https://gitlab.w00tserver.org/tflori/nb-sessions/badges/master/coverage.svg)](https://gitlab.w00tserver.org/tflori/nb-sessions/-/pipelines)
[![Latest Stable Version](https://poser.pugx.org/tflori/nb-sessions/v/stable.svg)](https://packagist.org/packages/tflori/nb-sessions) 
[![Total Downloads](https://poser.pugx.org/tflori/nb-sessions/downloads.svg)](https://packagist.org/packages/tflori/nb-sessions) 
[![License](https://poser.pugx.org/tflori/nb-sessions/license.svg)](https://packagist.org/packages/tflori/nb-sessions)

A non-blocking session handler for PHP. This library is inspired by 
[duncan3dc/sessions](https://github.com/duncan3dc/sessions).

## A word on non-blocking sessions

Sessions are by default blocking in php. And that is for a good reason: to prevent race conditions. When a session
is started by one request (to authenticate the user for example) another simultaneous request for this session
has to wait till the session is released. This way all what got written in the first request is available in the
second request.

When creating a custom session handler (a class that is handling session data). You can omit the locking mechanism
and some default handlers don't support locking (ini setting session.save_handler). Why that is not a solution and
this library is: session data is only read on session start and written at the end of the request.

When a second request comes in for the same session it reads the current state and writes the current state. Before
it writes to the session it reads the current data by (re-)starting the session. Of course one issue is left: it
does not read the data before each read. You should not rely on the data but a counter for example might require
the session to be refreshed before updating: `$session->counter = $session->refresh()->get('counter', 0) + 1`.

## Examples

### basic

```php
$session = new \NbSessions\SessionInstance();
$session->set('login', 'jdoe');
$login = $session->login;
```

### namespaces

To avoid key collisions you can use namespaces.

```php
$session->set('foo', 'bar');

$namespace = $session->getNamespace('my-module');
$namespace->set('foo', 'baz');

$session->foo; // 'bar'
$namespace->foo; // 'baz'
```

### static class

For easier access you can use the static class. But remember: it's harder to test.

```php
$namespace = \NbSessions\Session::getNamespace('my-module');
\NbSessions\Session::get('foo');
```

## Setup

Install it via composer and use without configuration.

```bash
composer require tflori/nb-sessions
```

### Options

The options are also taken from the php configuration. However, for convenience you can also pass them like that:

```php
$session = new \NbSessions\SessionInstance([
    'name' => 'PHPSESSID', // defaults to ini_get('session.name')
    'cookie_lifetime' => 0, // in seconds; defaults to ini_get('session.cookie_lifetime')
    'cookie_path' => '/', // ...
    'cookie_domain' => '',
    'cookie_secure' => false, // if you serving via ssl only set this to true
    'cookie_httponly' => false, // we highly recommend this to be true
    'cookie_samesite' => '',
    'destroyEmpty' => true, // when enabled the session gets automatically destroyed once the last element got deleted
]);
```

### Hints

This library does not overwrite the session configuration from php nor the registered session handler. All it does is
handling and forcing the use of cookies and writing the session each time after something got changed.

The session will not be started before something is stored or a cookie is provided and an element is read. This
prevents the application from starting useless requests to whatever session handler you are using.

As cookies will be handled by this library you don't have to worry about any cookies. It removes the cookie once
destroyed and updates the cookie when the lifetime is not 0 (0 means the cookie is stored only for the current
browser session).
