<?php

namespace NbSessions\Test;

use NbSessions\SessionInstance;

class GetAndSetTest extends TestCase
{
    /** @var SessionInstance */
    protected $session;

    protected function setUp(): void
    {
        parent::setUp();
        $this->session = new SessionInstance([], $this->phpWrapper);
    }

    /** @test */
    public function returnsNullForUnknownKeys()
    {
        $session = $this->session;

        $result = $session->get('foobar');

        self::assertNull($result);
    }

    /** @test */
    public function returnsTheDefaultValueForUnknownKeys()
    {
        $session = $this->session;

        $result = $session->get('count', 0);

        self::assertSame(0, $result);
    }

    /** @test */
    public function storesData()
    {
        $session = $this->session;

        $session->set('foo', 'bar');

        self::assertSame('bar', $session->get('foo'));
    }

    /** @test */
    public function storesDataToSession()
    {
        $session = $this->session;

        $this->phpWrapper->shouldReceive('sessionWriteClose')->once()->passthru();

        $session->set('foo', 'bar');
    }


    /** @test */
    public function setCanHandleArrays()
    {
        $session = $this->session;

        $this->phpWrapper->shouldReceive('sessionWriteClose')->once()->passthru();

        $session->set([
            'foo' => 'bar',
            'name' => 'John Doe'
        ]);

        self::assertSame('bar', $session->get('foo'));
        self::assertSame('John Doe', $session->get('name'));
    }

    /** @test */
    public function setUpdatesObjects()
    {
        $session = $this->session;
        $user = (object)['id' => 23, 'name' => 'John Doe'];
        $session->set('user', $user);

        $this->phpWrapper->shouldReceive('sessionWriteClose')->once()->passthru();

        $user->name = 'Jane Doe';
        $session->set('user', $user);

        self::assertSame('Jane Doe', $session->get('user')->name);
    }
}
