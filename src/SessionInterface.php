<?php

namespace NbSessions;

interface SessionInterface
{
    /**
     * Get value with $key from the session.
     *
     * @param string $key
     * @param mixed $default fallback value if key is not defined
     * @return mixed
     */
    public function get(string $key, $default = null);

    /**
     * Store value(s) in session.
     *
     * @param string|array $data Either the key or a key => value array to store
     * @param mixed $value If $data is a key then store this value in session
     * @return static
     */
    public function set($data, $value = null): self;

    /**
     * Delete $keys from session
     *
     * @param string ...$keys
     * @return static
     */
    public function delete(string ...$keys): self;

    public function __get(string $key);

    public function __set(string $key, $value);
}
